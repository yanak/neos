# Установка:
Действия выполняются (проверяли на 14 и 17 убунте также) в ОС Ubuntu 16.04:
   - Устанавливаем необходимые пакеты и зависимости:
   
```
     sudo apt-get update
     sudo apt-get install git mc vim nano wget curl nfs-common apt-transport-https ca-certificates
     sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
``` 

   - Для установки Docker в Ubuntu 16.04 необходимо добавить в систему официальный репозиторий Docker, а также GPG ключ доступа к нему:
   
```
     sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
     sudo echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
     sudo apt-get update
```

   - Установка docker-engine:
   
```
     sudo apt-get install docker-engine
```
 

   - Для запуска docker (и контейнеров) от обычного пользователя делаем следующее:
   
```
     sudo usermod -aG docker $USER
     sudo service docker restart
```

   - Перелогиниться в системе для применения изменений
     
   - Для установки docker-compose используем следующие команды:
   
```
     sudo curl -L https://github.com/docker/compose/releases/download/1.11.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
     sudo chmod +x /usr/local/bin/docker-compose
```

   - Проверить изменения можно запустив команду (без sudo):
   
```
     docker info
```

   - Создаем каталог www внутри /var и меняем владельца:
   
```
     sudo mkdir -p /var/www && sudo chown $USER:$USER /var/www/
```

   - Генерируем ssh-ключ для подключения в bitbucket:
   
```
     ssh-keygen -C "$(whoami)@$(hostname)"
```

   - Логинимся в bitbucket под своим пользователем и добавляем ssh-ключ (публичную часть ключа).
     Для получения публичной части ssh-ключа в консоли следует выполнить команду:
     
```
     cat ~/.ssh/id_rsa.pub
```

   - Клонируем проект:
   
```
     git clone ssh://git@bitbucket.org:yanak/neos.git /var/www/neos
```

   - Далее cd docker и там пользуемся командой make для развертывания проекта. Шаг первый:

```
     make prepare-project 
```
   - Шаг второй: изменяем файлы с переменными окружения (.env в корне проекта и .env в каталоге docker) под свои нужды. Например Если на хост-машине уже есть веб-сервер, который слушает порт, то можно в .env файле прописать порт в переменной HTTP_POSRT, например 81. В случае такой настройки, после выполнения последнего шага,  сайт можно вызывать по https://ed.local:81

  
   - Шаг третий: примените переменные окружения
   
```
     set -o allexport; . .env;
```

  - Шаг четвертый:

```          
     make create-project 
```
  

  
